from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel

from binary_network.choices import CHOICE_POSITION


class UserBinary(models.Model):
    user = models.ForeignKey(User, null=True, blank=False, verbose_name=_('User'),
                             help_text=_('Select user registered at the platform'), on_delete=models.SET_NULL)
    referral = models.ForeignKey(User, on_delete=models.CASCADE, null=True, related_name='direct', blank=False,
                                 verbose_name=_('Referral'),
                                 help_text=_('Select user direct or referral'))
    position = models.CharField(max_length=5, choices=CHOICE_POSITION, null=True, blank=False, default='left',
                                verbose_name=_('Position user'),
                                help_text=_('Select position where you want to add it'))
    date = models.DateTimeField(auto_now_add=True, verbose_name=_('Registration date'))

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name = _('User binary')
        verbose_name_plural = _('Users Binary')


class Tree(MPTTModel):
    user = models.ForeignKey(User, null=True, blank=False, verbose_name=_('User'),
                             help_text=_('Select user to add in the tree'), on_delete=models.SET_NULL)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='user_children', db_index=True,
                            default=0, on_delete=models.CASCADE)
    position = models.CharField(max_length=5, choices=CHOICE_POSITION, null=True, blank=False, default='left',
                                verbose_name=_('Position user'),
                                help_text=_('Select position where you want to add it'))

    class MPTTMeta:
        order_insertion_by = ['user']

    def __str__(self):
        return self.user.username if self.user else '{0}'.format(_('User deleted'))

    class Meta:
        verbose_name = _('Tree')
        verbose_name_plural = _('Tree')
