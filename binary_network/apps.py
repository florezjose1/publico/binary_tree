from django.apps import AppConfig


class BinaryNetworkConfig(AppConfig):
    name = 'binary_network'
