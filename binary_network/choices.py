from django.utils.translation import ugettext_lazy as _

CHOICE_POSITION = (
    ('left', _('Left')),
    ('right', _('Right'))
)
