import json
from django.http import HttpResponse

from binary_network.models import Tree
from binary_network.utils import pre_order_tree


def get_tree_user(request):
    json_data_tree = []
    node_tree = []

    root = Tree.objects.first()
    if root:
        pre_order_tree(node_tree, root, root.level, root.level, level_max=3)  # Data desk

    for node in node_tree:
        if node:
            json_data_tree.append({
                'id': str(node.user_id),
                'name': node.user.username,
            }, )
        else:
            json_data_tree.append({
                "id": '',
                "name": '',
            }),

    return HttpResponse(json.dumps(json_data_tree), status=200)
