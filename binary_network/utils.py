from binary_network.models import Tree


def pre_order_tree(nodes, item, initial_level, level, level_max):
    """Function of recursion for calculate the user in the tree until level specified

        Args:
            nodes (array): array for save of data
            item: node of evaluate
            initial_level: levels for search
            level: level of user
            level_max:  number max of level
        Returns:
            json of data
       """
    if level > initial_level + level_max:
        return
    nodes.append(item)
    left = Tree.objects.filter(parent_id=item.id, position="left").first() if item else None
    right = Tree.objects.filter(parent_id=item.id, position="right").first() if item else None

    if left or level <= initial_level + level_max:
        pre_order_tree(nodes, left, initial_level, level + 1, level_max)

    if right or level <= initial_level + level_max:
        pre_order_tree(nodes, right, initial_level, level + 1, level_max)


def get_level(level, parent, position):
    """Get level of last user added in tree

        Args:
            level ([]): array for save parent of new user
            parent (object<>): Object parent of user to check
            position (int): position in which has registered the new user

        Returns:
            short flow when parent is none or it does not have child
        """
    if parent is None:
        return

    level[0] = parent  # Save parent user temporarily

    # Check if exist to child of parend send
    child = Tree.objects.filter(parent_id=parent.id, position=position).first()
    if child:
        # If exist call again function where child happens to be parent
        get_level(level, child, position)

    return


def position_tree(user_id, position):
    """Position in the tree for the new user

        Args:
            user_id (int): id user of to behalf of who has registered
            position (int): position in which has registered the new user

        Returns:
            id parent in tree and level
        """
    tree = Tree.objects.filter(user_id=user_id).first()
    if tree is not None:
        level = [None]
        get_level(level, tree, position)

        pk = level[0].id
        n = level[0].level + 1
        return str(pk) + ":" + str(n)


def save_tree_user(referral_direct, father):
    """Save user in tree

        Args:
            referral_direct (object<>): Object of user
            father (int): Id user parent of user
        """
    data_level = position_tree(father, referral_direct.position)
    if data_level is not None:
        pk, level = data_level.split(':')

        save_tree = Tree(parent_id=int(pk), user_id=referral_direct.referral.id,
                         position=referral_direct.position,
                         level=int(level))
        save_tree.save()
