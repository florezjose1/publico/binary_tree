from django.urls import path, include
from binary_network import views as v

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.


urlpatterns = [
    path('binary-tree/', v.get_tree_user, name="get_binary_tree"),
]
