from django.contrib import admin
from mptt.admin import DraggableMPTTAdmin

from binary_network.models import Tree, UserBinary
from binary_network.utils import save_tree_user


@admin.register(Tree)
class TreeAdmin(DraggableMPTTAdmin):
    mptt_level_indent = 20
    fields = ['user', 'parent', 'position']

    list_filter = ('position',)
    search_fields = ('user__username',)


@admin.register(UserBinary)
class UserBinaryAdmin(admin.ModelAdmin):
    list_display = ('position', 'referral', 'date')
    list_filter = ('position',)
    search_fields = ('position', 'referral', 'date')

    def save_model(self, request, obj, form, change):
        values_tree = Tree.objects.filter(user_id=obj.referral.id)
        if len(values_tree) is 0:
            # Verified id parent exist in tree
            father = obj.user.id
            parent_tree = Tree.objects.filter(user_id=father)
            if not parent_tree:
                # Parent user will be the first user added at tree
                father = Tree.objects.first()
                if not father:
                    return
                father = father.user.id
            # Save user in tree
            save_tree_user(obj, father)
            obj.save()
