# Tree binary API

Versión Python: `^3.4`

Versión django: `2.2.5`


#### Configuración del proyecto

* Clone repositorio
```
$ git clone https://github.com/florezjose/binary_tree.git
$ cd binary_tree
```

* Cree virtulenv
```
$ virtualenv -p python3 env
$ source env/bin/activated
```

* Instale requerimientos
```
pip3 install -r requeriments.txt
```

* Ejecutar
```
python3 manage.py runserve
```


Hecho con ♥ por [Jose Florez](https://www.facebook.com/joserodolfo.florezortiz)
